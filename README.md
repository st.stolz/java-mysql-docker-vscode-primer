# java mysql docker vscode primer

## Setup

1. Clone Repo and open root folder in vscode
2. Install vscode "Remote - Containers" Extension
2. Select "View" - "Command Palette ..." in vscode and search "Remote Containers: Open Folder in Container ..."
3. MYSQL / Adminer should be up and running and you are logged in into the java container. 
4. Open the java file in the src folder and run it. 
5. Open http://ip-to-system-running-docker:8080 and you should see in adminer that table "STUDENTS" is created